# M08Curs1920

## Curs 2019-2020 de M08-Aplicacions Web

# UF1 - Ofimàtica i eines Web

**P1: Correu electrònic i Calendari Web**

*[Enunciat](/uf1/p1.md)*







**Curriculums dels alumnes:**

* **[Lennon](https://gitlab.com/15159795/m08/blob/master/curriculum.md)**
* **[Jordi](https://gitlab.com/46986035w/curriculum-jordi/blob/master/curriculum_markdown.md)**
* **[Achraf](https://gitlab.com/y5302499r/m08/blob/master/curriculum.md)**
* **[Santiago](https://gitlab.com/ar843234/modulo-08/blob/master/Curriculum.md)**
* **[Uiliam](https://gitlab.com/54987812x/m08-um/blob/master/curriculum.md)**
* **[Daniel](https://gitlab.com/45878162q/m08/blob/master/curriculum.md)**
* **[Jofre](https://gitlab.com/46980410n/m08/blob/master/curriculum.md)**


**Exemple de curriculum:**
[cv](https://gitlab.com/xsancho/m08curs1920/blob/master/curriculum.md)